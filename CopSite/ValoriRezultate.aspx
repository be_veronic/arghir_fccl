﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"  Title="Range valori rezultate" EnableTheming="true" CodeBehind="ValoriRezultate.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <asp:ScriptManager runat="server" EnablePageMethods="true" EnablePartialRendering="true"/>
   <asp:GridView ID="ValoriRezultateDetailsView"
                DataSourceID="fccl_v2"
                AutoGenerateRows="false"
               
                AutoGenerateEditbutton="true"
              
                 
                Gridlines="Both"
           
               RunAt="server">

                <HeaderStyle backcolor="Navy"
                  forecolor="White"/>

                <RowStyle backcolor="White"/>

                <AlternatingRowStyle backcolor="LightGray"/>

                <EditRowStyle backcolor="LightCyan"/>

                                 
              </asp:GridView>
          

        <asp:SqlDataSource ID="fccl_v2"  
          SelectCommand="SELECT Description, MinVal, MaxVal FROM Interval" 
          UpdateCommand="UPDATE Interval SET MinVal=@Minval, MaxVal=@Maxval, Updated=GETDATE()
                         WHERE Description=@Description"
          Connectionstring="<%$ ConnectionStrings:fccl_v2 %>" 
          RunAt="server">
       

             <UpdateParameters>
             <asp:Parameter Name="Description"   Type="String" />
            <asp:Parameter Name="MinVal"   Type="Double" />
            <asp:Parameter Name="MaxVal"  Type="Double" />
            <asp:Parameter Name="Id" Type="Int32" />

          </UpdateParameters>

    </asp:SqlDataSource>
       
    </asp:Content>
