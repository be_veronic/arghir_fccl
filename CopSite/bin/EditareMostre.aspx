<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="EditareMostre" EnableEventValidation="false" Title="Editare Mostre" EnableTheming="true" CodeBehind="EditareMostre.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(window).load(function () {
            $('#divLogDialogue').dialog({ width: ($(window).width() * 80) / 100, height: ($(window).height() * 80) / 100 });
            $('#divLogDialogue').dialog('close');
            $('#btnReceptie').click(function () {
                retrieveReceptionLog();
            });
            $('#btnRezultate').click(function () {
                retrieveResultsLog();
            });
        });

        function retrieveReceptionLog() {
            $.ajax({
                async: true,
                type: "POST",
                url: "EditareMostre.aspx/GetTodaysReceptionLog",
                data: "{}",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (data) {
                    $('#txtLog').val(data.d);
                    $('#divLogDialogue').dialog('open');
                },
                error: function (xhr, status, err) {
                    alert(status + ' ' + err + ' ' + xhr);
                }
            });
        }
        function retrieveResultsLog() {
            $.ajax({
                async: true,
                type: "POST",
                url: "EditareMostre.aspx/GetTodaysResultsLog",
                data: "{}",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (data) {
                    $('#txtLog').val(data.d);
                    $('#divLogDialogue').dialog('open');
                },
                error: function (xhr, status, err) {
                    alert(status + ' ' + err + ' ' + xhr);
                }
            });
        }
	</script>
    <asp:Panel ID="Panel1" runat="server" CssClass="paddingPanelsLogs">
        <input id="btnReceptie" type="button" value="Log import Receptie <%=DateTime.Now.ToString("dd/MM/yy")%>" />
        <input id="btnRezultate" type="button" value="Log import Rezultate <%=DateTime.Now.ToString("dd/MM/yy")%>" />
    </asp:Panel>
    <asp:Panel runat="server" Width="50%" CssClass="float-left">
        <asp:Panel runat="server" Width="90%" CssClass="float-left">
            <fieldset style="font-weight: bold;">
                <legend>Receptie</legend>
                <asp:FileUpload ID="FileUpload1" Width="50%" runat="server" />
                <asp:Button ID="Button3" Height="50px" runat="server" Font-Size="11px" width="150px" Text="Actualizare ferme" OnClick="update_ferme" />
                <asp:Button ID="ButtonRec" Height="50px" runat="server" Font-Size="11px" width="150px" Text="Incarca fisier receptie" OnClick="importa_receptie" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" width="10%" CssClass="float-right">
            <fieldset style="width: 160px; font-weight: bold; padding-left: 30px;">
                <legend>Rezultate</legend>
                <asp:Button ID="importm" runat="server" BackColor="#1abc9c" Font-Bold="True" Style="margin-bottom: 20px"
                    Font-Names="Arial" Font-Size="Small" Text="Import Manual"
                    OnClick="importm_Click" Width="140px" />
                <asp:Button ID="importap" runat="server" BackColor="#1abc9c" Font-Bold="True"
                    Font-Names="Arial" Font-Size="Small" Text="Import Aparate"
                    OnClick="importap_Click" Width="140px" />
            </fieldset>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="Panel3" CssClass="float-left paddingPanels" runat="server" Width="90%">
        <fieldset style="font-weight: bold">
            <legend>Cautare</legend>
            <asp:Panel CssClass="row" runat="server">
                <asp:Panel CssClass="col-1-8" runat="server">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Small"
                        ForeColor="SeaGreen" Text="Cod Bare:"></asp:Label>
                </asp:Panel>
                <asp:Panel CssClass="col-1-4" runat="server">
                    <asp:TextBox ID="cod" runat="server" Width="120px"></asp:TextBox>
                    <br />
                    <asp:RegularExpressionValidator ID="regCod" runat="server" ControlToValidate="cod" BackColor="Red" ErrorMessage="Codul trebuie sa fie numeric &lt;= 10 pozitii!!" Font-Size="8" ValidationExpression="\d{1,10}"></asp:RegularExpressionValidator>
                </asp:Panel>
                <asp:Panel CssClass="col-1-8" runat="server">
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Small"
                        ForeColor="SeaGreen" Text="Asociatii:"></asp:Label>
                </asp:Panel>
                <asp:Panel CssClass="col-1-4" runat="server">
                    <asp:DropDownList ID="ddlAsoc" runat="server" Width="350px">
                    </asp:DropDownList>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel CssClass="row" runat="server">
                <asp:Panel CssClass="col-1-8" runat="server">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Small"
                        ForeColor="SeaGreen" Text="Data testare initiala:"></asp:Label>
                </asp:Panel>
                <asp:Panel CssClass="col-1-4" runat="server">
                    <asp:TextBox ID="datatest1" class="useDatepicker" runat="server" Width="120px"></asp:TextBox>
                </asp:Panel>
                <asp:Panel CssClass="col-1-8" runat="server">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Small"
                        ForeColor="SeaGreen" Text="Data testare finala:"></asp:Label>
                </asp:Panel>
                <asp:Panel CssClass="col-1-4" runat="server">
                    <asp:TextBox ID="datatest2" class="useDatepicker" runat="server" Width="120px"></asp:TextBox>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel CssClass="row" runat="server">
                <asp:Panel CssClass="col-1-8" runat="server">
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Small"
                        ForeColor="SeaGreen" Text="NCS initial:"></asp:Label>
                </asp:Panel>
                <asp:Panel CssClass="col-1-4" runat="server">
                    <asp:TextBox ID="ncs1" runat="server" Width="120px"></asp:TextBox>
                </asp:Panel>
                <asp:Panel CssClass="col-1-8" runat="server">
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Small"
                        ForeColor="SeaGreen" Text="NCS final:"></asp:Label>
                </asp:Panel>
                <asp:Panel CssClass="col-1-4" runat="server">
                    <asp:TextBox ID="ncs2" runat="server" Width="120px"></asp:TextBox>
                </asp:Panel>

            </asp:Panel>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="DateValidate"
                ControlToValidate="datatest1" ErrorMessage="Data invalida!" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>

            <asp:CustomValidator ID="CustomValidator2" runat="server" ClientValidationFunction="DateValidate"
                ControlToValidate="datatest2" ErrorMessage="Data invalida!" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>

            <asp:Button ID="Button1" runat="server" BackColor="#1abc9c" Font-Bold="True"
                Font-Names="Arial" Font-Size="Small" OnClick="Button1_Click" Text="Cautare" Width="112px" />
            <asp:Button ID="Button2" runat="server" BackColor="#1abc9c" Font-Bold="True"
                Font-Names="Arial" Font-Size="Small" OnClick="Button2_Click" Text="Anulare filtre" Width="112px" />

        </fieldset>
    </asp:Panel>

    <br />
    <asp:Panel Height="50px" Width="90%" CssClass="float-left" runat="server">
        <asp:Button ID="Download_Results" Height="50px" CssClass="downloadButton float-right" runat="server" Font-Size="11px" OnClick="Download_Results_Click" Text="Download Tabel in Excel" />
        <br />
        <asp:Label ID="lcount" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Medium" SkinID="black" BackColor="#1ABC9C"
            Width="129px"></asp:Label>
        <asp:HyperLink ID="HyperLink1" runat="server" Font-Bold="True" Font-Names="Arial"
            ForeColor="SeaGreen" NavigateUrl="~/DetaliuMostra.aspx">Adaugare</asp:HyperLink>

    </asp:Panel>
    <asp:Panel ID="PanelE" runat="server" Width="90%">


        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="codbare"
            OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="200" CellPadding="2" CellSpacing="1" BorderColor="White" BorderStyle="Solid" GridLines="Vertical"
            OnRowDataBound="GridView1_RowDataBound" OnRowEditing="GridView1_RowEditing" Width="100%" AllowCustomPaging="True">
            <PagerStyle Font-Bold="True" Font-Names="Arial" Font-Size="Small" ForeColor="SeaGreen" />
            <Columns>
                <asp:BoundField DataField="id" HeaderText="ID" />
                <asp:BoundField DataField="codbare" HeaderText="Cod Bare" ItemStyle-Font-Bold="true" />
                <asp:BoundField DataField="rasa" HeaderText="Rasa" />
                <asp:BoundField DataField="datat" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data testare" />
                <asp:BoundField DataField="idzilnic" HeaderText="Id zilnic" />
                <asp:BoundField DataField="grasime" HeaderText="Grasime" />
                <asp:BoundField DataField="proteina" HeaderText="% Proteine" />
                <asp:BoundField DataField="casein" HeaderText="% Cazeina" />
                <asp:BoundField DataField="lactoza" HeaderText="% Lactoza" />
                <asp:BoundField DataField="substu" HeaderText="Subst. uscata" />
                <asp:BoundField DataField="ph" HeaderText="pH" />
                <asp:BoundField DataField="urea" HeaderText="Urea" />
                <asp:BoundField DataField="ncs" HeaderText="NCS" />
                <asp:CheckBoxField DataField="bdefinitiv" HeaderText="Definitiv" />
                <asp:CheckBoxField DataField="bvalidat" HeaderText="Validat" AccessibleHeaderText="Validat" />

                <asp:HyperLinkField HeaderText="Detalii..." Text="Detalii..." DataNavigateUrlFields="id"
                    DataNavigateUrlFormatString="~/DetaliuMostra.aspx?id={0}" />


            </Columns>
            <PagerSettings PageButtonCount="20" />
            <RowStyle BackColor="#009999" Font-Bold="False" Font-Names="Arial" Font-Size="9pt" BorderColor="White" BorderStyle="Solid" />
            <HeaderStyle BackColor="#3CB371" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
                ForeColor="White" BorderStyle="Solid" />
            <AlternatingRowStyle BackColor="#99FF99" />
        </asp:GridView>
        &nbsp;&nbsp;
        
    </asp:Panel>

    <div id="divLogDialogue" style="padding: 0 !important; overflow: hidden;">
        <textarea id="txtLog" readonly="readonly" style="width: 100%; height: 100%;"></textarea>
    </div>
</asp:Content>

